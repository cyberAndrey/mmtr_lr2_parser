import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class Parser {
    private XSSFWorkbook book;
    private XSSFSheet sheet;
    private JSONObject jsonObject;
    public ArrayList<String> data = new ArrayList<>();

    public Parser(String fileName) throws Exception {
        openBook(fileName);
        openSheet("data-20201005T050433-structure-");
        formJSONObject();
        formData();
        close();
    }

    private void openBook(final String path)
    {
        try {
            File file = new File(path);
            book = (XSSFWorkbook) WorkbookFactory.create(file);

        } catch (IOException e) {
            System.out.println("Не удалось открыть файл книги.");
        }
    }

    private void openSheet(String nameOfSheet) throws Exception {
        sheet = book.getSheet(nameOfSheet);
        if (sheet == null)
            throw new Exception("Не удалось открыть лист. Возможно название листа не совпадает с заданным.");
    }

    private void formJSONObject() {
        Iterator<Row> ri = sheet.rowIterator();
        XSSFRow row = (XSSFRow) ri.next();
        Iterator<Cell> ci = row.cellIterator();
        ArrayList<String> fields = new ArrayList<>();
        while(ci.hasNext()) {
            XSSFCell cell = (XSSFCell) ci.next();
            fields.add(cell.getRichStringCellValue().getString());
        }
        jsonObject = new JSONObject();
        for (String field : fields) jsonObject.put(field, null);
    }

    private void formData() {
        Iterator<Row> ri = sheet.rowIterator();
        String value;
        ArrayList<String> cells = new ArrayList<>();
        while (ri.hasNext()) {
            cells.clear();
            XSSFRow row = (XSSFRow) ri.next();
            Iterator<Cell> ci = row.cellIterator();
            while (ci.hasNext()) {
                value = "";
                XSSFCell cell = (XSSFCell) ci.next();
                switch (cell.getCellType()) {
                    case STRING:
                        value = cell.getRichStringCellValue().getString();
                        break;
                    case NUMERIC:
                        DataFormatter dataFormatter = new DataFormatter();
                        value = dataFormatter.formatCellValue(cell);
                        break;
                     case BLANK:
                        value = "-";
                        break;
                }
                cells.add(value);
            }
            String date = putJSON(cells);
            if (date.equals(Main.DATE)) this.data.add(jsonObject.toString());
        }
    }

    private String putJSON(ArrayList<String> fields) {
        /*
        Итератор пропускает пустые ячейки таблицы, а такие есть в 7ми записях.
        Пропуск либо в 3 и 5 ячейках, впервые встречается в строке с кодом 286759
        Либо в ячейке 7, впервые встречается в строке с кодом 358027
        Я решил заполнить это недоразумение вручную
         */
        if (fields.size() == 18) {
            fields.add(3, "-");
            fields.add(5, "-");
        } else if (fields.size() == 19) fields.add(7, "-");
        try {
            jsonObject.put("kod", fields.get(0));
            jsonObject.put("nomer_rasporjazhenija", fields.get(1));
            jsonObject.put("zakazchik", fields.get(2));
            jsonObject.put("inn_zakazchika", fields.get(3));
            jsonObject.put("podrjadchik", fields.get(4));
            jsonObject.put("inn_podrjadchika", fields.get(5));
            jsonObject.put("rajon", fields.get(6));
            jsonObject.put("adres", fields.get(7));
            jsonObject.put("vid_ogranichenija", fields.get(8));
            jsonObject.put("vid_rabot", fields.get(9));
            jsonObject.put("d.n.ogr", fields.get(10));
            jsonObject.put("d.o.ogr", fields.get(11));
            jsonObject.put("data_snjatija_ogr", fields.get(12));
            jsonObject.put("utochnenie_sroka", fields.get(13));
            jsonObject.put("dokumenty", fields.get(14));
            jsonObject.put("monitoring", fields.get(15));
            jsonObject.put("osnovanie", fields.get(16));
            jsonObject.put("psp", fields.get(17));
            jsonObject.put("msk", fields.get(18));
            jsonObject.put("adm_dela", fields.get(19));
        } catch (Exception e) {
            System.out.println(fields.get(0)  + "\tСтрока не спарсилась в json.");
        }
        return fields.get(11);
    }

    private void close() {
        try {
            book.close();
        } catch (IOException e) {
            System.out.println("Файл уже закрыт.");
        }
    }
}

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {
    public static String DATE = null;
    public static void main(String[] args) throws Exception {
        SimpleDateFormat format1 = new SimpleDateFormat("d M yyyy");
        SimpleDateFormat format2 = new SimpleDateFormat("yyyyMMdd");
        /*
        Еще пример парсера даты для строки типа: "Sat, April 3, 2020"
        SimpleDateFormat format = new SimpleDateFormat("EEE, MMMM d, yyyy", Locale.ENGLISH);
         */
        try {
            Date date = format1.parse(args[1]);
            DATE = format2.format(date);
        } catch (Exception e) {
            System.out.println("Недопустимый формат даты");
        }
        Process process = new Process();
        process.start();
        Parser parser = new Parser(args[0]);
        process.interrupt();
        process.join();
        if (parser.data.size() > 0)
        for (String s : parser.data) System.out.println(s);
        else System.out.println("Ни одной записи на текущую дату.");
    }

    static class Process extends Thread {
        @Override
        public void run() {
            while (!isInterrupted()) {
                System.out.print("processing ");
                for (int i = 0; i < 10; i++) {
                    try {
                        Thread.sleep(100);
                        System.out.print("." + " ");
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        System.out.println("\r");
                        break;
                    }
                }
                System.out.print("\r");
            }
        }
    }

}
